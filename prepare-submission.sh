#!/usr/bin/env bash

NAME=submission_test.zip
FINAL=ex1.zip

pushd src/chess
    zip "../../${NAME}" makefile chessSystem.c {match,matchId,playerStatistic,tournament}.{h,c} mtm_map/map.c
popd

cmake --build build --target dry-pdf
zip -j "${NAME}" build/dry.pdf

./finalcheck "${NAME}"

rm -i "${NAME}"
if [ -f "${NAME}" ]; then
    echo "Saving ${NAME} as ${FINAL}"
    mv "${NAME}" "${FINAL}"
fi
