#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include "map.h"

#define ORDERED_ENTRIES

#define HEAD_PLACEHOLDER (NULL)

typedef struct node_t
{
    MapKeyElement key;
    MapDataElement data;
    struct node_t* next;
} *Node;

struct Map_t {
    copyMapDataElements copy_data_elements;
    copyMapKeyElements copy_key_elements;
    freeMapDataElements free_data_elements;
    freeMapKeyElements free_key_elements;
    compareMapKeyElements compare_key_elements;
    unsigned int size;
    Node const head;
    Node current;
};

static void mapInvalidateIterator(Map map);
static Node mapFindLastSmaller(Map map, MapKeyElement key);
static Node mapFind(Map map, MapKeyElement key);
static Node nodeCreate(MapKeyElement key, MapDataElement data, Node next);

Map mapCreate(copyMapDataElements const copyDataElement,
              copyMapKeyElements const copyKeyElement,
              freeMapDataElements const freeDataElement,
              freeMapKeyElements const freeKeyElement,
              compareMapKeyElements const compareKeyElements)
{
    if (!(copyDataElement && copyKeyElement && freeDataElement
            && freeKeyElement && compareKeyElements)) {
        return NULL;
    }

    Map const map = malloc(sizeof(*map));
    if (!map) {
        return NULL;
    }

    Node const head = nodeCreate(HEAD_PLACEHOLDER, HEAD_PLACEHOLDER, NULL);
    if (!head) {
        free(map);
        return NULL;
    }

    memcpy((Node)&map->head, &head, sizeof(map->head));
    map->current = NULL;
    map->size = 0;
    map->copy_data_elements = copyDataElement;
    map->copy_key_elements = copyKeyElement;
    map->free_data_elements = freeDataElement;
    map->free_key_elements = freeKeyElement;
    map->compare_key_elements = compareKeyElements;
    return map;
}

void mapDestroy(Map const map)
{
    if (map) {
        mapClear(map);
        free(map->head);
        free(map);
    }
}

Map mapCopy(Map const map)
{
    if (!map) {
        return NULL;
    }
    Map const copy = mapCreate(map->copy_data_elements, map->copy_key_elements,
                               map->free_data_elements, map->free_key_elements,
                               map->compare_key_elements);
    if (!copy) {
        return NULL;
    }

    Node current = map->head->next;
    while (current) {
        // TODO avoid `mapPut`'s unecessary traversals by creating Nodes directly
        MapResult const error = mapPut(copy, current->key, current->data);
        if (MAP_SUCCESS != error) {
            mapDestroy(copy);
            mapInvalidateIterator(map);
            return NULL;
        }
        current = current->next;
    }

    mapInvalidateIterator(map);
    mapInvalidateIterator(copy);
    return copy;
}

int mapGetSize(Map const map)
{
    if (!map) {
        return -1;
    }

    assert(INT_MAX > map->size);
    return (int) map->size;
}

bool mapContains(Map const map, MapKeyElement const element)
{
    if (!(map && element)) {
        return false;
    }
    bool found = mapFind(map, element);
    mapInvalidateIterator(map);
    return found;
}

MapResult mapPut(Map const map, MapKeyElement const keyElement, MapDataElement const dataElement)
{
    // TODO invalidate iterator
    if (!(map && keyElement && dataElement)) {
        return MAP_NULL_ARGUMENT;
    }

    Node const last_smaller = mapFindLastSmaller(map, keyElement);
    assert(last_smaller);
    Node const expected = last_smaller->next;
    if (expected && !map->compare_key_elements(keyElement, expected->key)) {
        // The key exists
        MapDataElement const data_duplicate = map->copy_data_elements(dataElement);
        if (!data_duplicate) {
            return MAP_OUT_OF_MEMORY;
        }
        map->free_data_elements(expected->data);
        expected->data = data_duplicate;
        return MAP_SUCCESS;
    }

    assert(!expected || (0 > map->compare_key_elements(keyElement, expected->key)));
    // The key doesn't exist
    MapKeyElement const key_duplicate = map->copy_key_elements(keyElement);
    if (!key_duplicate) {
        return MAP_OUT_OF_MEMORY;
    }
    MapDataElement const data_duplicate = map->copy_data_elements(dataElement);
    if (!data_duplicate) {
        map->free_key_elements(key_duplicate);
        return MAP_OUT_OF_MEMORY;
    }
    Node const to_insert = nodeCreate(key_duplicate, data_duplicate, expected);
    if (!to_insert) {
        map->free_key_elements(key_duplicate);
        map->free_data_elements(data_duplicate);
        return MAP_OUT_OF_MEMORY;
    }
    last_smaller->next = to_insert;
    map->size++;
    return MAP_SUCCESS;
}

MapDataElement mapGet(Map const map, MapKeyElement const keyElement)
{
    if (!(map && keyElement)) {
        return NULL;
    }

    Node const target = mapFind(map, keyElement);
    return target ? target->data : NULL;
}

MapResult mapRemove(Map const map, MapKeyElement const keyElement)
{
    if (!(map && keyElement)) {
        return MAP_NULL_ARGUMENT;
    }

    Node const last_smaller = mapFindLastSmaller(map, keyElement);
    assert(last_smaller);
    Node const expected = last_smaller->next;
    if (expected && !map->compare_key_elements(keyElement, expected->key)) {
        last_smaller->next = expected->next;
        expected->next = NULL;
        map->free_key_elements(expected->key);
        map->free_data_elements(expected->data);
        free(expected);

        map->size--;
        mapInvalidateIterator(map);
        return MAP_SUCCESS;
    }

    assert(!expected || (0 > map->compare_key_elements(keyElement, expected->key)));
    mapInvalidateIterator(map);
    return MAP_ITEM_DOES_NOT_EXIST;
}

MapKeyElement mapGetFirst(Map const map)
{
    if (!map) {
        return NULL;
    }
    map->current = map->head;
    assert(map->current);
    return mapGetNext(map);
}

MapKeyElement mapGetNext(Map const map)
{
    if (!(map && map->current)) {
        return NULL;
    }
    map->current = map->current->next;
    if (map->current) {
        MapKeyElement const key = map->copy_key_elements(map->current->key);
        if (!key) {
            // The only reasonable deterministic solution is to invalidate the iterator
            // TODO libmap seems not to do this?
            // mapInvalidateIterator(map);
            return NULL;
        }
        return key;
    }
    return NULL;
}

MapResult mapClear(Map const map)
{
    if (!map) {
        return MAP_NULL_ARGUMENT;
    }

    Node current = map->head->next;
    while (current) {
        Node const to_remove = current;
        current = current->next;
        map->free_key_elements(to_remove->key);
        to_remove->key = NULL;
        map->free_data_elements(to_remove->data);
        to_remove->data = NULL;
        free(to_remove);
        map->size--;
    }

    assert(!map->size);
    map->head->next = NULL;
    mapInvalidateIterator(map);
    return MAP_SUCCESS;
}

/**
 * mapInvalidateIterator: Used after operations that potentially
 *      break iteration to makes the iterator state deterministic.
 * @param map - The map to invalidate.
 */
static void mapInvalidateIterator(Map const map)
{
    map->current = NULL;
}

/**
 * mapFindLastSmaller: Finds the node to insert {@param key} after.
 *
 * @param map - The Map to search.
 * @param key - The search target.
 * @return
 *      On success: The {@typedef Node} to insert {@param key} after.
 *      On null argument: NULL.
 */
static Node mapFindLastSmaller(Map const map, MapKeyElement key)
{
    if (!(map && key)) {
        return NULL;
    }

    Node current = map->head;
    Node next = current->next;
    while (next && (0 < map->compare_key_elements(key, next->key))) {
        current = next;
        next = next->next;
    }

    assert(current && ((map->head == current) || (current->key
        && (0 < map->compare_key_elements(key, current->key)))));
    assert(!next || (0 >= map->compare_key_elements(key, next->key)));
    return current;
}

/**
 * mapFind: Finds the node that holds {@param key} if such node exists.
 *
 * @param map - The Map to search.
 * @param key - The search target.
 * @return
 *      On success: The {@typedef Node} that holds {@param key}
 *      On any failure: NULL
 */
static Node mapFind(Map const map, MapKeyElement const key)
{
    if (!(map && key)) {
        return NULL;
    }

    Node const last_smaller = mapFindLastSmaller(map, key);
    assert(last_smaller && ((last_smaller == map->head) || (0 < map->compare_key_elements(key, last_smaller->key))));
    Node const expected = last_smaller->next;
    if (expected && !map->compare_key_elements(key, expected->key)) {
        return expected;
    }
    assert(!expected || (0 > map->compare_key_elements(key, expected->key)));
    return NULL;
}

/**
 * nodeCreate: Allocates a new Node.
 *
 * @param key - The key to hold.
 * @param data - The data to hold.
 * @param next - The next Node to hold.
 * @return
 *      On success - The newly allocated Node.
 *      On allocation failure - NULL.
 */
static Node nodeCreate(MapKeyElement const key, MapDataElement const data,
                       Node const next)
{

    Node const node = malloc(sizeof(*node));
    if (!node) {
        return NULL;
    }
    node->key = key;
    node->data = data;
    node->next = next;
    return node;
}
