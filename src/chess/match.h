#ifndef MATCH_H
#define MATCH_H

#define MATCH_PLAYER_COUNT (2)

/** Type for representing a match between two players */
typedef struct match_t * Match;

/**
 * matchCreate: creates a match along with its data.
 *
 * @param first_player - the ID of the first player in the match
 * @param second_player - the ID of the second player in the match
 * @param winner - an enum representing the index of the winner of the match
 * @param play_time - the amount of time the game took
 * @param error - a ChessResult enum to specify errors
 * @return a new match in case of success. NULL otherwise with the error specified in the
 * parameter error.
 */
Match matchCreate(int const first_player, int const second_player,
                  Winner const winner, int const play_time,
                  ChessResult *error);

/**
 * matchDestroy: free a chess system, and all its contents, from
 * memory.
 *
 * @param match - the match to free from memory. A NULL value is
 *     allowed, and in that case the function does nothing.
 */
void matchDestroy(Match match);

/**
 * matchCopy: copies a match into a new one.
 *
 * @param match - the match that will be copied. Cannot be NULL.
 * @return The copied match in case of success.NULL in case of allocation error.
 */
Match matchCopy(Match match);

/**
 * matchValidatePlayerID: validates the ID of the player.
 *
 * @param player_id - the ID which will be validated .
 * @return true if the ID is valid and false otherwise.
 */
bool matchValidatePlayerID(int player_id);

/**
 * matchValidatePlayTime: validates the play time of the match
 *
 * @param play_time - the play time to be validated
 * @return true if the play time is valid, false otherwise.
 */
bool matchValidatePlayTime(int play_time);

/**
 * matchRemovePlayer: marks the player as removed in the match.
 *
 * @param match - match that contains the player. Must be non-NULL.
 * @param player_id - the player id. Must be positive.
 *
 * @return
 *     true - if the player has was removed successfully
 *     false - possible internal failures:
 *          CHESS_NULL_ARGUMENT - if match is NULL.
 *          CHESS_INVALID_ID - if the player ID number is invalid.
 *          CHESS_PLAYER_NOT_EXIST - if the player does not exist in the match.
 *          CHESS_SUCCESS - if player was removed successfully.
 */
bool matchRemovePlayer(Match match, int player_id);

/**
 * matchNoPlayersWereRemoved: checks if players were removed from the match or not
 *
 * @param match - the match which is being checked
 * @return true if no players were removed and false otherwise.
 */
bool matchNoPlayersWereRemoved(Match match);

/**
 * matchGetPlayTime: returns the play time of the match.
 *
 * @param match - the match whose play time will be returned.
 * @return the play time of the match, an integer.
 */
int matchGetPlayTime(Match match);

/**
 * matchGetStatistics: returns the scoring of the requested player in the match.
 *
 * @param match - the match from which data will be extracted.
 * @param player_index - the index of the player in the match whose data will be extracted
 * @return the data of the requested player in the match in the form of the
 * playerStatistics data type.
 */
PlayerStatistic matchGetStatistics(Match match, bool player_index);

/**
 * matchIsMatchOf: checks if a match contains a player.
 *
 * @param match - the the match.
 * @param player_id - the id of the player.
 * @return true if the player is in the match (and not removed) and false otherwise.
 */
bool matchIsMatchOf(Match match, int player_id);

#endif // MATCH_H
