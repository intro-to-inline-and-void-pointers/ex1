#ifndef TOURNAMENT_H
#define TOURNAMENT_H

/** Type for representing a chess tournament that organizes chess matches */
typedef struct tournament_t * Tournament;

/**
 * tournamentCreate: create an empty tournament.
 *
 * @return A new tournament in case of success, and NULL otherwise alongside a chessResult
 * pointer that specifies the error
 */
Tournament tournamentCreate(int id, char const * location,
                            int max_games_per_player, ChessResult *error);
/**
 * tournamentDestroy: free a tournament, and all its contents, from
 * memory.
 *
 * @param tournament - the tournament to free from memory.
 */
void tournamentDestroy(Tournament tournament);

/**
 * tournamentCopy: copies a tournament into another tournament.
 *
 * @param tournament - the tournament that will be copied.
 * @return a Tournament with the same parameters as tournament, and NULL otherwise
 */
Tournament tournamentCopy(Tournament tournament);

/**
 * tournamentGetID: returns the ID of the tournament
 *
 * @param tournament - the tournament whose ID will be returned.
 * @return the ID of the tournament, an integer.
 */
int tournamentGetID(Tournament tournament);

/**
 * tournamentGetLocation: returns the location of the tournament
 *
 * @param tournament - the tournament whose location will be returned.
 * @return the location of the tournament, a char const *.
 */
char const * tournamentGetLocation(Tournament tournament);

/**
 * tournamentGetMaxGamesPerPlayer : returns the max amount of games per player
 * in the tournament.
 *
 * @param tournament - the tournament whose amount of max games will be returned.
 * @return the max amount of games allowed in the tournament, an unsigned integer.
 */
int tournamentGetMaxGamesPerPlayer(Tournament tournament);

/**
 * tournamentValidateTournamentID: validates the ID of the tournament.
 *
 * @param tournament_id - the ID which will be validated
 * @return a false value if the ID is valid and a true value otherwise.
 */
bool tournamentValidateTournamentID(int tournament_id);

/**
 * tournamentValidateLocation: validates the location of the tournament.
 *
 * @param location - the location which will be validated
 * @return a false value if the location is valid and a true value otherwise.
 */
bool tournamentValidateLocation(char const * location);

/**
 * tournamentValidateMaxGames: validates the max amount of games in a tournament.
 *
 * @param max_games_per_player - the number to be validated
 * @return true if that is a valid amount of max games allowed and false otherwise.
 */
bool tournamentValidateMaxGames(int max_games_per_player);

/**
 * tournamentAddGame: add a new match to a chess tournament.
 *
 * @param tournament - tournament in a chess system. Must be non-NULL.
 * @param first_player - first player id. Must be positive.
 * @param second_player - second player id. Must be positive.
 * @param winner - indicates the winner in the match. if it is FIRST_PLAYER,
 *                 then the first player won. if it is SECOND_PLAYER, then
 *                 the second player won, otherwise the match has ended with
 *                 a draw.
 * @param play_time - duration of the match in seconds. Must be non-negative.
 *
 * @return
 *     CHESS_NULL_ARGUMENT - if tournament is NULL.
 *     CHESS_INVALID_ID - if the players or the winner is invalid or both players
 *                        have the same ID number.
 *     CHESS_TOURNAMENT_ENDED - if the tournament already ended
 *     CHESS_GAME_ALREADY_EXISTS - if there is already a game in the tournament
 *                                  with the same two players (both were not removed).
 *     CHESS_INVALID_PLAY_TIME - if the play time is negative.
 *     CHESS_EXCEEDED_GAMES - if one of the players played the maximum number of games allowed
 *     CHESS_SUCCESS - if game was added successfully.
 */
ChessResult tournamentAddGame(Tournament tournament, int first_player,
                              int second_player, Winner winner,
                              int play_time);

/**
 * tournamentRemovePlayer:
 *     Removes the player from the tournament.
 *     In games where the player has participated and not yet ended,
 *     the opponent is the winner automatically after removal.
 *     If both player of a game were removed, the game still exists in the tournament.
 *
 * @param tournament - tournament that contains the player. Must be non-NULL.
 * @param player_id - the player id. Must be positive.
 *
 * @return
 *     true - if the player has was removed successfully
 *     false - possible internal failures:
 *          CHESS_NULL_ARGUMENT - if tournament is NULL.
 *          CHESS_INVALID_ID - if the player ID number is invalid.
 *          CHESS_PLAYER_NOT_EXIST - if the player does not exist in the system.
 *          CHESS_TOURNAMENT_ENDED - if the tournament has already ended.
 *          CHESS_SUCCESS - if player was removed successfully.
 */
bool tournamentRemovePlayer(Tournament tournament, int player_id);

/**
 * tournamentEnd:
 *      The function will end the tournament if it has at least one game and
 *      calculate the id of the winner.
 *      The winner of the tournament is the player with the highest score:
 *      player_score = ( num_of_wins * 2 + num_of_draws * 1 ) / ( num_of_games_of_player )
 *      If two players have the same score, the player with least losses will be chosen.
 *      If two players have the same number of losses, the player with the most wins will be chosen
 *      If two players have the same number of wins and losses,
 *      the player with smaller id will be chosen.
 *      Once the tournament is over, no games can be added for that tournament.
 *
 * @param tournament- tournament that will end. Must be non-NULL.
 *
 * @return
 *     CHESS_NULL_ARGUMENT - if tournament is NULL.
 *     CHESS_TOURNAMENT_ENDED - if the tournament already ended
 *     CHESS_N0_GAMES - if the tournament does not have any games.
 *     CHESS_SUCCESS - if tournament was ended successfully.
 */
ChessResult tournamentEnd(Tournament tournament);

/**
 * tournamentAggregatePlayerStatistics:
 *      collects and returns all relevant data on players in the tournament.
 *
 * @param tournament - the tournament whose player scoring will be returned.
 * @return a map of the relevant data on players who played in the tournament.
 */
Map tournamentAggregatePlayerStatistics(Tournament tournament);

/**
 * tournamentIsEnded: checks if the tournament has ended.
 *
 * @param tournament - the tournament that will be checked
 * @return true if the tournament has ended and false otherwise.
 */
bool tournamentIsEnded(Tournament tournament);

/**
 * tournamentSaveStatistics:
 *      prints to the file the statistics for the tournament as explained in the *.pdf
 *
 * @param tournament - a tournament. Must be non-NULL.
 * @param path_file - the file path which within it the tournament statistics will be saved.
 * @return
 *     CHESS_NULL_ARGUMENT - if tournament is NULL.
 *     CHESS_NO_TOURNAMENTS_ENDED - if the tournament hasn't ended.
 *     CHESS_SAVE_FAILURE - if an error occurred while saving.
 *     CHESS_SUCCESS - if the ratings were printed successfully.
 */
ChessResult tournamentSaveStatistics(Tournament tournament, char *path_file);

#endif // TOURNAMENT_H
