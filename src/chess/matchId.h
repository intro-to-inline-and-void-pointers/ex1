#ifndef MATCH_ID_H
#define MATCH_ID_H

typedef struct match_id_t *MatchId;

/**
 * matchIdCreateFirst: creates the first match between two players.
 * @param first_player - the id of the first player.
 * @param second_player - the id of the second player.
 * @return the match id of the match created.
 */
MatchId matchIdCreateFirst(int first_player, int second_player);

/**
 * matchIdCreateNewer: creates a match that is not the first between the players
 * @param id - the id of the previous match.
 * @return the new match id.
 */
MatchId matchIdCreateNewer(MatchId id);

/**
 * matchIdDestroy: frees the match struct.
 * @param id - the id of the match to be freed.
 */
void matchIdDestroy(MatchId id);

/**
 * matchIdCopy: copies a match map key.
 *
 * @param id - the match map key to be copied.
 * @return the copy of the match id.
 */
MatchId matchIdCopy(MatchId id);

/**
 * matchIdCompare: compares two match ids.
 *
 * @param left - one of the match ids to be compared.
 * @param right - one of the match ids to be compared.
 * @return returns a positive value if left is greater,
 * zero if they are the same and a negative number otherwise.
 */
int matchIdCompare(MatchId left, MatchId right);

/**
 * matchIdLastMatchBetween: finds the last match that occured between two players.
 * @param first_player - the id of the first player.
 * @param second_player - the id of the second player.
 * @param matches - the map of matches between the players.
 * @return the match id of the last match between them.
 */
MatchId matchIdLastMatchBetween(int first_player, int second_player, Map matches);

/**
 * matchIdCanPlayersPlayAnotherMatch: checks if the two players can have a match.
 * @param id - the id of the match that contains the players
 * @param matches - a map of relevant matches.
 * @param max_games - the number of max games allowed.
 * @return
 *     CHESS_NULL_ARGUMENT - if id or matches is NULL.
 *     CHESS_INVALID_MAX_GAMES - if max_games is invalid.
 *     CHESS_EXCEEDED_GAMES - if they cannot have another game.
 *     CHESS_SUCCESS - if they can and the function worked as expected.
 */
ChessResult matchIdCanPlayersPlayAnotherMatch(MatchId id, Map matches, int max_games);

#endif // MATCH_ID_H
