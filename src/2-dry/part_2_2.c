#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

char * foo(char const * const str, int * const x)
{
    assert(NULL != str);
    int const length = strlen(str);
    if (NULL != x) {
        *x = length;
    }
    char * const reversed = malloc(sizeof(*reversed) * (length + 1));
    if (NULL == reversed) {
        return NULL;
    }
    for (int i = 0; i < length; ++i) {
        reversed[i] = str[length - 1 - i];
    }
    reversed[length] = '\0';
    printf("%s\n", length % 2 ? str : reversed);
    return reversed;
}

int main(int const argc, char const * const * const argv)
{
    if (2 != argc) {
        fprintf(stderr, "Usage: %s <string>\n", argv[0]);
        return 1;
    }
    char * const reversed = foo(argv[1], NULL);
    int length = -1;
    char * const reversed2 = foo(argv[1], &length);
    printf("*x = %d\n", length);
    free(reversed);
    free(reversed2);
    return 0;
}
