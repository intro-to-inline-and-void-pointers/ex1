#include <gtest/gtest.h>

extern "C" {
    #include "../chess/mtm_map/map.h"

    #include "../chess/chessSystem.h"
    #include "../chess/playerStatistic.h"
    #include "../chess/tournament.h"
}

TEST(Tournament, TestCreateDestroy) {
    ChessResult error = CHESS_SUCCESS;
    Tournament const tournament = tournamentCreate(1, "A", 3, &error);
    ASSERT_EQ(CHESS_SUCCESS, error);
    ASSERT_TRUE(tournament);
    tournamentDestroy(tournament);
}

TEST(Tournament, TestAggregateStatistice) {
    GTEST_SKIP_("Not implemented yet");
    ChessResult error = CHESS_SUCCESS;
    Tournament const tournament = tournamentCreate(1, "A", 3, &error);
    ASSERT_EQ(CHESS_SUCCESS, error);
    ASSERT_TRUE(tournament);

    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 1, 2, FIRST_PLAYER, 2000));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 1, 3, FIRST_PLAYER, 3000));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 3, 2, SECOND_PLAYER, 3000));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 4, 1, SECOND_PLAYER, 1000));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 2, 4, FIRST_PLAYER, 3500));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 3, 4, DRAW, 400));

    Map const aggregate = tournamentAggregatePlayerStatistics(tournament);
    ASSERT_EQ(4, mapGetSize(aggregate));

    int const player1 = 1;
    PlayerStatistic statistics1 = (PlayerStatistic) mapGet(aggregate, (void *) &player1);

    // TODO CONTINUE

    mapDestroy(aggregate);
    tournamentDestroy(tournament);
}

TEST(Tournament, TestExceededGames) {
    ChessResult error = CHESS_SUCCESS;
    Tournament const tournament = tournamentCreate(1, "A", 2, &error);
    ASSERT_EQ(CHESS_SUCCESS, error);
    ASSERT_TRUE(tournament);
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 1, 2, FIRST_PLAYER, 7));
    ASSERT_EQ(CHESS_GAME_ALREADY_EXISTS, tournamentAddGame(tournament, 1, 2, FIRST_PLAYER, 7));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 1, 3, FIRST_PLAYER, 7));
    ASSERT_EQ(CHESS_EXCEEDED_GAMES, tournamentAddGame(tournament, 1, 4, FIRST_PLAYER, 7));
    tournamentDestroy(tournament);
}

TEST(Tournament, TestExceededGamesWithRemovedPlayers) {
    ChessResult error = CHESS_SUCCESS;
    Tournament const tournament = tournamentCreate(1, "A", 2, &error);
    ASSERT_EQ(CHESS_SUCCESS, error);
    ASSERT_TRUE(tournament);

    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 1, 2, FIRST_PLAYER, 1));
    ASSERT_EQ(CHESS_GAME_ALREADY_EXISTS, tournamentAddGame(tournament, 1, 2, FIRST_PLAYER, 1));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 1, 3, FIRST_PLAYER, 1));
    ASSERT_EQ(CHESS_EXCEEDED_GAMES, tournamentAddGame(tournament, 1, 4, FIRST_PLAYER, 1));
    ASSERT_EQ(CHESS_SUCCESS, tournamentAddGame(tournament, 2, 3, FIRST_PLAYER, 1));
    ASSERT_EQ(CHESS_EXCEEDED_GAMES, tournamentAddGame(tournament, 2, 4, FIRST_PLAYER, 1));
    ASSERT_TRUE(tournamentRemovePlayer(tournament, 1));
    ASSERT_EQ(CHESS_EXCEEDED_GAMES, tournamentAddGame(tournament, 2, 4, FIRST_PLAYER, 1));
    ASSERT_EQ(CHESS_EXCEEDED_GAMES, tournamentAddGame(tournament, 2, 1, FIRST_PLAYER, 1));
    ASSERT_EQ(CHESS_EXCEEDED_GAMES, tournamentAddGame(tournament, 2, 4, FIRST_PLAYER, 1));

    tournamentDestroy(tournament);
}
